#include <algorithm>
#include <initializer_list>
#include <iostream>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

/////////////// C++ 11 ///////////////
// https://en.cppreference.com/w/cpp/11

// auto and decltype
// static assertions

void auto_and_decltype() {
    int x = 0;
    int &xref = x;
    [[gnu::unused]]
    auto a1 = xref;
    static_assert(std::is_same<decltype(a1), int>::value, "type mismatch");
    // !!! a1 is int, not int&

    // see further down for auto and decltype in C++14
}

// defaulted and deleted functions (since C++11, however compiles with -std=c++98 with warning)

struct S {
    S() = default;
    // want to make as base class for polimorphic subclasses, but don't want to type a lot
    virtual ~S() = default;
    // disallow copy (move-only class)
    S(const S&) = delete;
    S& operator=(const S&) = delete;
};

// final and override (since C++11, however compiles with -std=c++98 with warning)

struct Final {
    virtual ~Final() = default;
    virtual void foo() final; // but why virtual then (makes maybe sense in a subclass)?
    virtual void bar();
};

struct Override: public Final {
    //void foo(); // error: virtual function ‘virtual void Override::foo()’ overriding final function
    void bar() override;
    //void baz() override; // error: ‘void Override::baz()’ marked ‘override’, but does not override
};

// trailing return type

template<typename T>
auto trailing_return(const T &a, const T &b) -> decltype(a+b) {
    return a + b;
}

// rvalue references and move semantics

struct Moveable {
    Moveable(Moveable &&other) { /* move stuff from other */
    }
    Moveable& operator=(Moveable &&other) { /* move stuff from other */
        return *this;
    }
    ;
};

void rvalue_references(Moveable &&m) {
    std::vector<Moveable> v;
    v.push_back(std::move(m));
    v.emplace_back(std::move(m)); // for tg
}

// scoped enums (since C++11, however compiles with -std=c++98 with warning)

enum Colors {
    white, black, gray
};
enum Options {
    yes, no, maybe
};
enum class SafeColors {
    red, blue, green
};

void scoped_enums() {
    Options o = yes;
    if (o == black) { /* do something */
    } // wtf? but compiler accepts (warns at least: -Wenum-compare)
    //if ( o == SafeColors::blue ) { /* do something */ } //  error: no match for ‘operator==’ (operand types are ‘Options’ and ‘SafeColors’)
    [[gnu::unused]]
    int i = yes;
    //int j = SafeColors::red; // error: cannot convert ‘SafeColors’ to ‘int’ in initialization
}

// constexpr

int constexpr can_be_calculated_by_compiletime() {
    return 42;
}

// list initialization and attributes

struct InitList {
    InitList(int) {
    }
    ;
    InitList (std::initializer_list<double>) { };
};

void testInitList() {
    [[gnu::unused]]
    InitList a(1);
    [[gnu::unused]]
    InitList b = 1;
    [[gnu::unused]]
    InitList c { 1 }; // !!! initializer_list is prefered over normal initialization.
                      //     here initializer_list initialization is used after implcit conversion of 1 to double!
    [[gnu::unused]]
    InitList d = { 1 }; // !!! dito
    [[gnu::unused]]
    InitList e = { 1.1, 2.2, 3.3 };
}

// delegating and inherited constructors (since C++11, however compiles with -std=c++98 with warning)

struct B {
    B(int a) {
    }
    ;
    B(const std::string &s);
};

struct D: public B {
    D(int a, int b) :
            B(a) {
    }
    ; // delegating
    using B::B;
    // inheriting from B D(int) and D(const string&)
};

// brace-or-equal initializers

class X {
public:
    X(int i, std::string s) :
            i(i), s(std::move(s)) {
    }
    ;
    int i;
    std::string s;
};

void brace_or_equal_init() {
    X x = { 1, "x" };
}

// nullptr

void foo(int i) {
}
;
void foo(void *p) {
}
;

void nullPointerType() {
    foo(nullptr); // no disambiguity
}

// type aliases

using myint = int;

// range based for

template<typename ContainerType>
void increase_elements(ContainerType &container) {
    for (typename ContainerType::value_type &value : container) {
        value++;
    }
}

void instantiate() {
    std::vector<int> v;
    increase_elements(v);
}

// lambda expressions (with some C++11 auto return)

auto extract_even_values(std::vector<int> v) -> decltype(v) {
    auto isEven = [](int i) {
        return i % 2 == 0;
    };
    std::vector<int> result;
    std::copy_if(begin(v), end(v), std::back_inserter(result), isEven);
    return result;
}

// variadic templates (since C++11, however compiles with -std=c++98 with warning)

template<class ... Types> struct Tuple {
};
Tuple<> t0;             // Types contains no arguments
Tuple<int> t1;          // Types contains one argument: int
Tuple<int, float> t2;   // Types contains two arguments: int and float
//Tuple<0> error;       // error: 0 is not a type

// Unicode string literals

const char *utf8string = u8"15€ geht";

// user-defined literals

class Distance {
public:
    Distance(double d) :
            d(d) {
    }
    ;
private:
    double d = 0; // in meters
};

Distance operator "" _km(long double km) {
    return Distance(1000.0 * km);
}
Distance operator "" _m(long double m) {
    return Distance(m);
}

void user_defined_literals() {
    [[gnu::unused]]
    Distance x = 4.2_km; // x.d is now 4200.0
}

// noexcept

void does_not_throw() noexcept; // at least I say that (exception specifier not part of function type in C++11)
void may_throw() noexcept(false);

// alignof and alignas

void align() {
    alignas(16) int a;
    int b;
    static_assert(alignof(a) == 16, "size mismatch");
    static_assert(alignof(b) == 4, "size mismatch");
}

// thread-local storage

thread_local int i; // there is a separate instance of i for each thread

// Nothing to show...

// long long, char16_t and char32_t
// generalized unions
// generalized PODs
// multithreaded memory model
// GC interface (GC not implemented in any common C++ Compiler)

/////////////// C++ 14 ///////////////
// https://en.cppreference.com/w/cpp/14

// auto and decltype revisited

#if __cplusplus >= 201402L

void auto_and_decltype_revisited() {
    // recall...
    int x = 0;
    int &xref = x;
    auto a1 = xref;
    static_assert(std::is_same<decltype(a1), int>::value, "type mismatch");
    // !!! a1 is int, not int&
    // new in C++14
    decltype(auto) a2 = xref;
    static_assert(std::is_same<decltype(a2), int&>::value, "type mismatch");
    // now a2 is int&
}

// variable templates

template<class T>
constexpr T pi = T(3.1415926535897932385L);

template<class T>
T circular_area(T r) {
    return pi < T > *r * r;
}

// generic (polymorphic) lambdas
// and
// return type deduction for functions

auto extract_even_values_revisited(
        std::vector<int> v) /* see, no "-> decltype(v)" here! */{
    std::vector<int> result;
    std::copy_if(begin(v), end(v), std::back_inserter(result), [](auto i) {
        return i % 2 == 0;
    }); // auto as lambda parameter type
    return result;
}

// lambda captures expressions

int more_lambdas() {
    int x = 4;
    int y = [&rx = x, x = x + 1]() { // compiles with -std=c++11 with warning
                rx += 2;
                return x * x;
            }();
    return x + y;
} // returns 29 (4 + 25); the lambda updates local variable x to 6 and initializes y to 25.

// relaxed restrictions on constexpr functions

constexpr int factorial(int n) {
    int result = 1;
    while (n > 1) {
        result *= n--;
    }
    return result;
} // in C++11: error: body of ‘constexpr’ function ‘constexpr int factorial(int)’ not a return-statement

// binary literals

int fourtytwo = 0b101010; // since C++14, compiles however also with -std=c++11

// digit separators

int million = 1'000'000;

// aggregate initialization for classes with brace-or-equal initializers.

#endif

/////////////// C++ 17 ///////////////
// https://en.cppreference.com/w/cpp/17

#if __cplusplus >= 201703L

// fold-expressions

template<typename ... Args>
bool all(Args ... args) {
return (... && args);} // since C++17, compiles however also with -std=c++11

bool b = all(true, true, true, false);

// class template argument deduction

void class_template_argument_deduction() {
    std::pair p(2, 4.5);     // deduces to std::pair<int, double> p(2, 4.5);
    std::tuple t(4, 3, 2.5); // same as auto t = std::make_tuple(4, 3, 2.5);
}

// auto non-type template parameters

template<auto n> struct AutoTemplateParams { /* ... */
};
AutoTemplateParams<5> a1;   // n is int
AutoTemplateParams<'a'> a2; // n is char
// AutoTemplateParams<2.5> a3; // error: non-type template parameter type cannot be double

template<auto...> struct VariadicAutoTemplateParams {
};
VariadicAutoTemplateParams<'C', 0, 2L, nullptr> variadicAutoTemplateParams;

// compile-time if constexpr

extern int x; // x is not defined anywhere !!!

int if_constexpr_example() {
    if constexpr (true)
        return 0;
    else if (x) // links without problems, because the else branch never gets it into the compiled code
            return x;
        else
            return -x;
}

// inline variables
// This examples shows only the syntax. An inline variable does not make
// any sense in a .cc file. Imagine, this line is in a header. Then the
// variable "counter" would have external linkage in the given name space,
// independent of how many times it would have been included into .cc files.

inline int counter = 0;

// structured bindings

std::pair<std::string, int> structured_return_value() {
    return std::pair { "result value", 0 };
}

void structured_bindings() {
    auto [result_value, result_status] = structured_return_value();
    if (result_status != 0) {
        std::cerr << "Failed with status " << result_status << std::endl;
        return;
    }
    std::cout << result_value << std::endl;
}

// initializers for if and switch

void check_token(const std::string &token) {
    if (auto keywords = { "if", "for", "while" }; std::any_of(begin(keywords),
        end(keywords), [&token](const char *kw) {
            return token == kw;
        })) {
        std::cerr << "Token must not be a keyword\n";
    }

    switch (size_t s = token.size()) {
        case 1:
            s++; // do something (s++ just to eliminate -Wunused)
            break;
        default:
            ;    // do something
    }
}

// u8-char

char euro = u8'a';// I don't get it. ("If c-char is not representable with a single UTF-8 code unit, the program is ill-formed.")

// simplified nested namespaces
// What does "simplified" mean?

// using-declaration can declare multiple names

namespace A {
    namespace B {
        void g();
        void h();
    }
    namespace C {
        void i();
        using B::g, B::h;
    }
}

// made noexcept part of type system

void ft() {
} // potentially-throwing
//void (*fn)() noexcept = ft; // until C++17 this compiles, since C++17 two functions with the same signature but differen noexcept specifier differ in type
                              // error: invalid conversion from ‘void (*)()’ to ‘void (*)() noexcept’ [-fpermissive]

// lambda capture of *this

struct CaptureThisObject {
    int x = 0;
    int foo() {
        return [*this]() { bar(); return x + 1; }();
    }
    void bar() const {

    }
};

// constexpr lambda

auto constexpr_fourtytwo = []() {
    return 42;
}; // a lambda ist automatically a constexpr, if possible
static_assert(constexpr_fourtytwo() == 42);

auto non_constexpr_fourtytwo = []() {
    static int fourtytwo = 42;
    return fourtytwo;
};
//static_assert(non_constexpr_fourtytwo() == 42); // error: ‘fourtytwo’ declared ‘static’ in ‘constexpr’ function

// attribute namespaces don't have to repeat

[[gnu::unused, gnu::weak]]  // repeated attribute namespaces
int attribute_example1;
[[using gnu: unused, weak]] // the big difference
int attribute_example2;

// new attributes [[fallthrough]] [[nodiscard]] and [[maybe_unused]].

[[nodiscard]]
int nodiscard_function(int x) {
    [[maybe_unused]]
    int unused = 42;
    switch (x) {
        case 1:
            x++;
            [[fallthrough]] // I did not forget to break
        case 2:
            x++;
            break;
        default:
            break;
    }
    return x;
}

void call_nodiscard_function() {
    nodiscard_function(42); // warning: ignoring return value of ‘int nodiscard_function(int)’, declared with attribute nodiscard [-Wunused-result]
}

// __has_include
// Preprocessor constant expression that evaluates to 1 if the file name is found and ​0​ if not.
// The program is ill-formed if the argument would not be a valid argument to the #include directive.

#if __has_include(<optional>)
#  include <optional>
#  define have_optional 1
#elif __has_include(<experimental/optional>)
#  include <experimental/optional>
#  define have_optional 1
#  define experimental_optional 1
#else
#  define have_optional 0
#endif

// <any>

// <optional>

// <variant>

// <string_view>


// Nothing to show...
// guaranteed copy elision
/*
 // new order of evaluation rules
 E.g:
 until C++17:
 The call to the allocation function (operator new) is indeterminately sequenced with respect to the evaluation of the constructor arguments in a new-expression.
 since C++17:
 The call to the allocation function (operator new) is sequenced before the evaluation of the constructor arguments in a new-expression.
 */

#endif

int main(int argc, char **argv) {
    return 0;
}
